## ps2dev-repo
A complete playstation 2 development environment using git and repo

### Get the sources
Create a directory for the development environment
```
mkdir ps2max
cd ps2max
```
Initialze the repo
This is only needed once
```
# Option1: init for git cloning the entire git repositories
repo init -u https://gitlab.com/ps2max/ps2dev-repo.git -b ps2max

# Option2: init for git cloning using --depth=1 (shalow clones, no history, faster)
repo init -u https://gitlab.com/ps2max/ps2dev-repo.git -b ps2max --depth=1
```
Get all sources (this will take some time)
Repeat this step every time you would to get the latest sources
```
# Option1: sync all
repo sync -j4

# Option2: sync only current branch, do not sync tags (faster)
repo sync -j4 -c --no-tags
```

### Set the environment variables
First you need to remove all old environment variables to $PS2DEV,$PS2SDK,etc... if you have any.
Then execute the following command to set the environment variables.
NOTE: you must execute this command every time you open a new terminal.
```
. envsetup.sh
```

### Build
The build-all script is a good first start.
```
./build-all.sh
```
